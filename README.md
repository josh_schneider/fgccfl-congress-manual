# FGCCFL Student Congress Manual #

This is the Student Congress (Congressional Debate) manual for the Florida Gulf Coast Catholic Forensics League. It contains the rules of the event, guidelines for bills and resolutions, tabulation procedures, and information for judges (Scorers and Parliamentarians). The manual will be posted in multiple sections for convenience as well as a single-page printable version.

## Okay, why is this on Bitbucket? ##

To put it simply, we'd like to maintain a complete version history of the manual, and we'd like to allow coaches to be able to propose and discuss rule adjustments and interpretations without flooding our Congress Coordinator's inbox.